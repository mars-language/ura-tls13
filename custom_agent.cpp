/* micro-ROS-related libraries */
#include <uxr/agent/transport/custom/CustomAgent.hpp>
#include <uxr/agent/transport/endpoint/IPv4EndPoint.hpp>

/* standard and tcp-related libraries */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <poll.h>
#include <signal.h>

/* wolfSSL-related libraries */
#include <wolfssl/options.h>
#include <wolfssl/ssl.h>
#include "certs/tls_buffers.h"

#define STREAM true
#define PACKET false

#define AGENT_PORT 8888

int                sockfd;
int                connd;
struct sockaddr_in servAddr;
struct sockaddr_in clientAddr;
socklen_t          size = sizeof(clientAddr);


/* declares wolfSSL objects */
WOLFSSL_CTX* ctx = NULL;
WOLFSSL*     ssl = NULL;

void mem_cleaner(bool c_connd){
    if (c_connd)
        close(connd);           /* closes the connection to the client        */
    wolfSSL_CTX_free(ctx);      /* frees the wolfSSL context object           */
    wolfSSL_Cleanup();          /* cleans up the wolfSSL environment          */
    wolfSSL_free(ssl);          /* frees the wolfSSL object                   */
    close(sockfd);              /* closes the socket listening for clients    */
}

int main(int argc, char** argv)
{
    eprosima::uxr::Middleware::Kind mw_kind(eprosima::uxr::Middleware::Kind::FASTDDS);
    uint16_t agent_port(AGENT_PORT);

    struct pollfd pfds;

    eprosima::uxr::CustomAgent::InitFunction init_function = [&]() -> bool
    {
        bool rv = true;
        int ret;
        /* creates a socket that uses an internet IPv4 address */
        if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
            fprintf(stderr, "[error] » failed to create the socket.\n");
            rv = false;
        }

        /* creates and initializes WOLFSSL_CTX */
        if ((ctx = wolfSSL_CTX_new(wolfSSLv23_server_method())) == NULL) {
            fprintf(stderr, "[error] » failed to create WOLFSSL_CTX.\n");
            rv = false;
        }

        /* loads server certificates into WOLFSSL_CTX */
        if ((ret = wolfSSL_CTX_use_certificate_buffer(ctx, server_cert_der_2048, sizeof_server_cert_der_2048, WOLFSSL_FILETYPE_ASN1)) != WOLFSSL_SUCCESS) { 
            fprintf(stderr, "[error] » failed to load certificate, please check the buffer.\n");
            rv = false;
        }

        /* loads server key into WOLFSSL_CTX */
        if ((ret = wolfSSL_CTX_use_PrivateKey_buffer(ctx, server_key_der_2048, sizeof_server_key_der_2048, WOLFSSL_FILETYPE_ASN1)) != WOLFSSL_SUCCESS) {
            fprintf(stderr, "[error] » failed to load private key, please check the buffer.\n");
            rv = false;
        }

        /* initializes the server address struct with zeros */
        memset(&servAddr, 0, sizeof(servAddr));

        /* fills in the server address */
        servAddr.sin_family      = AF_INET;             /* using IPv4      */
        servAddr.sin_port        = htons(agent_port);   /* on DEFAULT_PORT */
        servAddr.sin_addr.s_addr = INADDR_ANY;          /* from anywhere   */

        do {
            /* binds the server socket to our port */
            if (bind(sockfd, (struct sockaddr*)&servAddr, sizeof(servAddr)) == -1) {
                fprintf(stderr, "[error] » failed to bind.\n");
                mem_cleaner(true);
                rv = false;
            }

            /* listens for new connections, allowing 5 pending connections */
            if (listen(sockfd, 5) == -1) {
                fprintf(stderr, "[error] » failed to listen.\n");
                mem_cleaner(true);
                rv = false;
            }

            if ((connd = accept(sockfd, (struct sockaddr*)&clientAddr, &size)) == -1) {
                fprintf(stderr, "[error] » failed to accept the connection.\n");
                rv = false;
                mem_cleaner(false);
            }

            /* creates a WOLFSSL object */
            if ((ssl = wolfSSL_new(ctx)) == NULL) {
                fprintf(stderr, "[error] » failed to create WOLFSSL object.\n");
                ret = -1;
                rv = false;
            }

        } while (rv == false);

        /* attaches wolfSSL to the socket */
        wolfSSL_set_fd(ssl, connd);


        /* establishes TLS connection */
        ret = wolfSSL_accept(ssl);
        if (ret != WOLFSSL_SUCCESS) {
            fprintf(stderr, "[error] » wolfSSL_accept error = %d\n", wolfSSL_get_error(ssl, ret));
            rv = false;
        }

        if(rv == false)
            mem_cleaner(true);
        return rv;
    };

    eprosima::uxr::CustomAgent::FiniFunction fini_function = [&]() -> bool
    {
        mem_cleaner(true);
        return true;
    };

    eprosima::uxr::CustomAgent::RecvMsgFunction recv_msg_function = [&](
            eprosima::uxr::CustomEndPoint* source_endpoint,
            uint8_t* buffer,
            size_t buffer_length,
            int timeout,
            eprosima::uxr::TransportRc& transport_rc) -> ssize_t
    {
        ssize_t bytes_received = 0;

        /* wolfssl max buffer size is int */
        if(buffer_length > __INT_MAX__) buffer_length = __INT_MAX__;

        pfds.fd = wolfSSL_get_fd(ssl);
        pfds.events = POLLIN;

        int pending = wolfSSL_pending(ssl);
        int poll_rv = 0;

        if (pending >= buffer_length || 0 < (poll_rv = poll(&pfds, 1, timeout)))
        {
            ssize_t r_read = wolfSSL_read(ssl, buffer, buffer_length);

            if (r_read < 0)
            {
                transport_rc = eprosima::uxr::TransportRc::connection_error;
            }
            else
            {
                transport_rc = eprosima::uxr::TransportRc::ok;
                bytes_received = r_read;
                source_endpoint->set_member_value<uint32_t>("address", clientAddr.sin_addr.s_addr);
                source_endpoint->set_member_value<uint16_t>("port", static_cast<uint16_t>(clientAddr.sin_port));
            }
        }
        else
        {
            transport_rc = (0 == poll_rv) ? eprosima::uxr::TransportRc::timeout_error : eprosima::uxr::TransportRc::server_error;
            std::cout << "[erro] » read timeout " << timeout  << " ms for " << buffer_length << " pending: " << pending << std::endl;
        }
        return bytes_received;
    };

    eprosima::uxr::CustomAgent::SendMsgFunction send_msg_function = [&](
        const eprosima::uxr::CustomEndPoint* destination_endpoint,
        uint8_t* buffer,
        size_t message_length,
        eprosima::uxr::TransportRc& transport_rc) -> ssize_t
    {
        int ret = 0;

        ssize_t bytes_sent = wolfSSL_write(ssl, buffer, message_length);

        if (bytes_sent < 0) 
        {
            printf("[erro] » failed to write on the transport.\n");
            transport_rc = eprosima::uxr::TransportRc::server_error;
        }
        else
        {
            transport_rc = eprosima::uxr::TransportRc::ok;
            ret = (size_t) bytes_sent;
        }
        return ret;
    };

    /** runs the main application */
    try
    {
        /* initializes wolfSSL */
        wolfSSL_Init();

        /** EndPoint definition for this transport. We define an address and a port. */
        eprosima::uxr::CustomEndPoint custom_endpoint;
        custom_endpoint.add_member<uint32_t>("address");
        custom_endpoint.add_member<uint16_t>("port");

        /** creates a custom agent instance. */
        eprosima::uxr::CustomAgent custom_agent(
            "wolf-ag",
            &custom_endpoint,
            mw_kind,
            STREAM,
            init_function,
            fini_function,
            send_msg_function,
            recv_msg_function);

        /** sets verbosity level */
        custom_agent.set_verbose_level(6);

        /** runs agent and wait until an stop signal is received */
        custom_agent.start();

        int n_signal = 0;
        sigset_t signals;
        sigwait(&signals, &n_signal);

        /** stops agent, and exit. */
        custom_agent.stop();
        return 0;
    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << std::endl;
        return 1;
    }
}