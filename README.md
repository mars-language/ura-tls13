# micro-ROS Agent (TLS 1.3 Transport)

The micro-ROS Agent (µRA) is a server that connects micro-ROS Clients (µRCs) to a DDS Domain. The µRA acts on behalf of the µRCs by publishing on the DDS domain messages the µRC wanted to publish, and forwarding to the µRC messages related to the topics µRC subscribes.

This repository goes through the steps to compile and run a µRA using our custom transport based on TLS 1.3.

## Installation

To install and run our custom transport, you will need a Ubuntu 20.04 machine and the following software installed on it:

### ROS2
The version of ROS2 used in this project is Galactic Geochelone. You can find the step-by-step tutorial on how to install it [here](https://docs.ros.org/en/galactic/Installation/Ubuntu-Development-Setup.html).

### wolfSSL
This project uses version 5.1.0 of wolfSSL. You can download wolfSSL [here](https://www.wolfssl.com/download/). Please follow the building tutorial found [here](https://www.wolfssl.com/docs/wolfssl-manual/ch2/) without adding and removing any of the default features.

### eProsima's Micro-XRCE-DDS-Agent
Our custom transport is designed compatible with eProsima's Micro-XRCE-DDS Agent. You can clone their implementation in a folder of your choice.

```bash
git clone -b develop https://github.com/eProsima/Micro-XRCE-DDS-Agent.git
```
Now that you have a folder for the Agent, clone this repository into the examples folder, located at `<your_path>/Micro-XRCE-DDS-Agent/examples`. You will end up with the following folders:

```bash
custom_agent uras-tls13
```

## Configuring the Environment
After downloading eProsima's Micro-XRCE-DDS Agent, a few extra configuration steps are required on the `CMakeLists.txt` located at `<your_path>/Micro-XRCE-DDS-Agent/`:

* Search for `UAGENT_BUILD_USAGE_EXAMPLES` in the document and turn this option ON 
```cmake
option(UAGENT_BUILD_USAGE_EXAMPLES "Build Micro XRCE-DDS Agent built-in usage examples" ON)
```
* Search for `UAGENT_BUILD_USAGE_EXAMPLES` in the document and add this repository's folder
```cmake
# Examples
if(UAGENT_BUILD_USAGE_EXAMPLES)
    add_subdirectory(examples/custom_agent) # default folder for custom agents
    add_subdirectory(examples/ura-tls13) # folder of our custom agent 
endif()
```
* Search for `add_library(${PROJECT_NAME} ${SRCS})` and right below it, add `find_library(LIBWOLFSSL_L wolfssl)`:
```cmake
...
add_library(${PROJECT_NAME} ${SRCS})
find_library(LIBWOLFSSL_L wolfssl)
...
```
* Search for `target_link_libraries` and on the public section, add `${LIBWOLFSSL_L}`
```cmake
...
target_link_libraries(${PROJECT_NAME}
    PUBLIC
        fastcdr
        $<$<PLATFORM_ID:Windows>:ws2_32>
        $<$<PLATFORM_ID:Windows>:iphlpapi>
        $<$<BOOL:${UAGENT_LOGGER_PROFILE}>:spdlog::spdlog>
        ${LIBWOLFSSL_L}
    PRIVATE
        $<$<BOOL:${UAGENT_FAST_PROFILE}>:fastrtps>
        $<$<BOOL:${UAGENT_P2P_PROFILE}>:microxrcedds_client>
        $<$<BOOL:${UAGENT_P2P_PROFILE}>:microcdr>
        $<$<PLATFORM_ID:Linux>:pthread>
    )
...
```
* Search for `arget_include_directories(${PROJECT_NAME}` and on the public section, add `/src/local/include`
```cmake
...
target_include_directories(${PROJECT_NAME} BEFORE
    PUBLIC
        $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
        $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include>
        $<INSTALL_INTERFACE:${INCLUDE_INSTALL_DIR}>
        $<TARGET_PROPERTY:fastcdr,INTERFACE_INCLUDE_DIRECTORIES>
        /src/local/include
    PRIVATE
        $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src/cpp>
        $<$<BOOL:${UAGENT_FAST_PROFILE}>:$<TARGET_PROPERTY:fastrtps,INTERFACE_INCLUDE_DIRECTORIES>>
        $<$<BOOL:${UAGENT_LOGGER_PROFILE}>:$<TARGET_PROPERTY:spdlog::spdlog,INTERFACE_INCLUDE_DIRECTORIES>>
        $<$<BOOL:${UAGENT_P2P_PROFILE}>:$<TARGET_PROPERTY:microxrcedds_client,INTERFACE_INCLUDE_DIRECTORIES>>
        $<$<BOOL:${UAGENT_P2P_PROFILE}>:$<TARGET_PROPERTY:microcdr,INTERFACE_INCLUDE_DIRECTORIES>>
    )
...
```

## Known Issues
 
## License
[GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)